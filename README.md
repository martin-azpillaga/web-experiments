# Web experiments

Each branch in this repository represents an experiment I have made while deepening my understanding of web development.

## Licensing

All the code in all branches is licensed under the [AGPLv3] license.

[AGPLv3]: https://opensource.org/licenses/agpl-3.0
